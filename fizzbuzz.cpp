// Fizzbuzz:
// - Prints the numbers 1 to 100
// - Prints "Fizz" after the number for multiples of 3
// - Prints "Buzz" after the number for multiples of 5
// - Prints "FizzBuzz" after the number for multiples of 3 and 5

#include <iostream>
using namespace std;

int main()
{
  for(int i=1; i<=100; i++)
    {
      cout << i << " ";
      if(i%3==0)
      {
        cout << "Fizz";
      }
      if(i%5==0)
      {
        cout << "Buzz";
      }
      cout << endl;
    }
  return 0;
}
