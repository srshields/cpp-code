// Towers of Hanoi
// A program to simulate the Towers of Hanoi game in the command prompt/terminal
// This game will:
// - Prompt the user for how many disks will be used in the game
// - Loop the game, asking the user for 2 inputs (pegs to move from and to)
// - Each loop the game will take the user's input and determine whether it is a legal move
//   if the move is legal, it will execute it, otherwise, it will display a message and prompt the user
//   to retry
// - Once the game has been completed, it will display a message and prompt the user to repeat
// - (Optional) The game will display each step as ASCII art

#include <iostream>
#include <vector>
#include <array>
#include <string>

using namespace std;

// Prototypes:
bool can_move(int peg_from, int peg_to, array<vector<int>,3> const& pegs);
bool game_over(array<vector<int>,3> const& pegs);

int main()
{
  //----------------------------------------------------
  // Declarations:
  string NUM_DISKS_str;
  cout << "How many disks would you like to use? ";
  cin >> NUM_DISKS_str;
  const int NUM_DISKS = stoi(NUM_DISKS_str);
  string peg_from_str = "";
  string peg_to_str = "";
  // Each peg is represented by an array  of ints, each int representing a disk size
  // These arrays are stored in an array of size 3, one entry for each peg
  array<vector<int>,3> pegs{{vector<int>(NUM_DISKS),
        vector<int>(0),
        vector<int>(0)}};
  // Initialize the first peg with all of the disks.
  for(int i=0;i<NUM_DISKS;i++)
    {
      pegs[0][i] = NUM_DISKS - i;
    }
  //---------------------------------------------------

  // GAME MASTER LOOP:
  while(true)
    {
      // Print what the current pegs look like:
      for(int i=0;i<3;i++)
        {
          cout << "Peg " + to_string(i+1) + " : ";
          for(int j=0;j<pegs[i].size();j++)
            {
              cout << pegs[i][j] << " ";
            }
          cout << endl;
        }
      // Prompt the user:
      cout << "What peg would you like to move from? (1/2/3) ";
      cin >> peg_from_str;
      cout <<  "What peg would you like to move to? (1/2/3) ";
      cin >> peg_to_str;
      int peg_from = stoi(peg_from_str) - 1;
      int peg_to = stoi(peg_to_str) - 1;

      if (can_move(peg_from, peg_to, pegs))
        {        // move the disk
          int moving_disk = pegs[peg_from].back();
          pegs[peg_from].pop_back();
          pegs[peg_to].push_back(moving_disk);
        }
      else
        {
          cout << "You cannot make that move, try again" << endl;
        }
      if(game_over(pegs))
        {
          cout << "Congratulations! You won!\n";
          return 0;
        }
    }
  return 0;
}

// Determines if the playe can move the disk to the desired peg
bool can_move(int peg_from, int peg_to, array<vector<int>,3> const& pegs)
{
  if(pegs[peg_from].size())    // If the from_peg is not empty:
    {
      if(!pegs[peg_to].size()) // If the to_peg is empty, we can move
         {
           return true;
         }                      // If the to_peg top disk is larger than the from_peg, we can move
      else if(pegs[peg_to].back() > pegs[peg_from].back())
                 {
                   return true;
                 }              // otherwise, we cannot move
    }
  return false;
}

// Determines when the game is finished
bool game_over(array<vector<int>,3> const& pegs)
{
  if(!pegs[0].size())     // If the first peg is empty
    {
      if(!pegs[1].size() || !pegs[2].size())
        {                 // If one of the other two pegs is also empty
          return true;    // This means the disks were successfully moved
        }
    }
  return false;           // Otherwise, the game isn't over yet
}
